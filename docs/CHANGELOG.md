# Changelog

### [1.0.6](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/compare/release/1.0.5...release/1.0.6) (2024-10-14)


### Bug Fixes

* **labels:** change appName as dynamic variable ([3df1b00](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/3df1b00f333b903dd12ca7aec087605932206289))


### Continuous Integration

* **tag testing prerelease:** generate numbered testing versions ([070dd9f](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/070dd9f9b1bae9a568a8f9921d5845f2a474370f))

### [1.0.5](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/compare/release/1.0.4...release/1.0.5) (2024-06-17)


### Bug Fixes

* **libraries:** update Meteor and npm libraries ([7596657](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/759665783ea0eaa900dab5921778f5ed28c06457))

### [1.0.4](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/compare/release/1.0.3...release/1.0.4) (2024-01-30)


### Bug Fixes

* **audit:** update vulnerable libraries ([c848610](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/c8486100f6165be99673a4afd5560b2b5732ea4c))

### [1.0.3](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/compare/release/1.0.2...release/1.0.3) (2023-08-24)


### Bug Fixes

* **lint:** update eslint settings ([fc65745](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/fc65745ed892f65478b2c56c3b1644cfbe5a3954))

### [1.0.2](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/compare/release/1.0.1...release/1.0.2) (2023-05-05)


### Bug Fixes

* **settings:** remove enableKeycloak setting (Keycloak is mandatory) ([9f90a29](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/9f90a29f73fc713bd437c593c31e004f366cc130))

### [1.0.1](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/compare/release/1.0.0...release/1.0.1) (2022-09-16)


### Bug Fixes

* **login:** refactor login/logout process ([ee68b70](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/ee68b701669d9bb16ac0d667ebed21878d321015))

## 1.0.0 (2022-07-04)


### Features

* **404:** add 404 page when nclocator doesn't exist ([92b713e](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/92b713ee9ef5735f6bd9a263540ebff5a8ad563f))
* **ci:** add gitlab ci ([9e18e30](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/9e18e3037f61ef920ae5634c79ea3f85b70d72d8))
* **git:** add gitignore ([c642b19](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/c642b192e5bd5cb0b2fbcfad7c263e098158e0aa))
* **global:** publish nclocator, fix some ui issues ([d536501](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/d536501a50530499d80b32d1dad5de99420b28e4))
* **global:** simplify code and omit schema ([8f8a947](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/8f8a947b1276b9325eb99774d480ddd353b31cd8))
* **url:** format url with protocol when doesn't exist ([5ac0766](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/5ac076616ff1f8492e16be27608fbb6729da1660))
* **workflow:** add keycloak again, logout when redirecting ([0f06fda](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/0f06fdafacf08772e311cc380ded5431b71c6119))


### Bug Fixes

* **audit:** update vulnerable librairies ([30db0e0](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/30db0e0a088950d5a9068673152467d42bb5b750))
* **ci:** fix ci ([97c7788](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/97c7788fa57dcd7d4dcff2ba1ec4a3f6d29f752d))
* **ci:** fix meteor version in ci ([9247931](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/9247931dd8b361363506b69f9ae3c3bed2e40fa5))
* **ci:** fix tag ([f3f2d36](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/f3f2d365c773423b7e8dc12d1a9c76831c3b07c1))
* **cleanup:** code cleanup, disable client account creation ([fcd5e76](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/fcd5e765ac578a51f6221a383d1228d9fc03b28f))
* **docker:** adapt docker to app settings ([b945445](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/b94544515486e4992bb568a64502fdcf683e470b))
* **format:** add http verification ([20a2ef9](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/20a2ef9952c8a643692beb9d1a3a480dffaa24e6))
* **global:** clean code ([0ab4d56](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/0ab4d564265a50f5d6795d0fb7458daebed92f1c))
* **global:** clean code ([ff76891](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/ff768916582af19e9622ffd6989a9f8fe13cd62f))
* **redirect:** fix redirection when nclocator is undefined ([0b4ea8f](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/0b4ea8f49c0814a4a79094f6d0a1fd7a55e4a926))
* **redirect:** use alternate redirect method on logout ([78f0068](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/78f0068db7c2c6894a6ef9c4c3e424872b90cce2))
* **server:** fix exception on hmac generation, add .gitignore ([9ec3caa](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/9ec3caa45d38625c4b245dc353d173d1936af356))
* **settings:** fix settings and gitignore ([8e1114f](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/8e1114f3e35ad4483ed20b3a9d29eab1514d1b8c))
* **settings:** remove settings ([2560364](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/256036432c229e1d26180283b41e46d130fab6b6))
* **url:** fix redirection url ([0c50d83](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/0c50d834669e3c15db396c1725ca6936ab7238ad))


### Code Refactoring

* **global:** delete themes ([0466ac8](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/0466ac89a07ff37732e4efdfc50655ef43289006))
* **global:** refactor code ([7bbaa6f](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/7bbaa6fec180b3f013f3dfca577d43f7f2237dcd))
* **global:** refactor code ([19eb525](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/19eb52534110d8fc06ba85cb8bf8244698ec71fa))
* **redirection:** improve redirection after login ([e00f49e](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/e00f49e56dbc8af82e7a9b9dd494fb4039977bf0))


### Documentation

* **config:** add laboiteUrl in config README ([8cf3b46](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/8cf3b46a22afe9256d3361d4971064666edd4164))


### Continuous Integration

* **build-docker:** build on `dev` even for `semantic-release` commits ([ce0d8c2](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/ce0d8c2359f177d52731e95a3869631e38346f2e))
* **build-docker:** use new standard jobs `.docker:image:build` ([eecd113](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/eecd1139da562dadb218f4e1e43bfd11b0f5afa3))
* **commitlint:** use new standard job `.git:commitlint` ([d791c5a](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/d791c5a1741badc058d09c36b9ea7d4c953c2acd))
* **commitlint:** use new standard jobs `.semantic-release:*` ([bf39f2e](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/bf39f2eba6aa40ce579e5d8b899a336b13668746))
* **merge-to-dev:** use new standard jobs `.git:merge-to` ([98d8c9b](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/98d8c9bc0e0142c31585d0a973e0d3802f744b57))
* **meteor:** test before generating a new release ([df944cd](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/df944cd1681a9d5cb26d25ec079a18d4e11ad781))
* **tag docker:** use new standard jobs `.docker:image:tag` ([ca55f47](https://gitlab.mim-libre.fr/alphabet/frontal-nextcloud/commit/ca55f471e89cbef5c67720f42d2df6845a2d658f))
