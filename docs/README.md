# Application **Frontal-nextcloud** en environnement DEV

Sommaire :

- Installation
  - Application
  - Paramètres
- Lancer le projet

## Installation

### Application : frontal-nextcloud

Procédure d'nstallation :

```
git clone git@gitlab.mim-libre.fr:alphabet/frontal-nextcloud.git
cd frontal-nextcloud
cp config/settings.development.json.sample config/settings.development.json
cd app
meteor npm install
```

## Lancer le projet

En développment, le projet nécessite d'avoir une application laboite lancée simultanément.

cd laboite/app
meteor npm start

```
cd frontal-nextcloud/app
meteor npm run start-dev
```

Il est possible de vérifier le fonctionnement de laboite en tapant la ligne suivante à partir d'un navigateur.

```
http://localhost:3000
```

Il est possible de vérifier le fonctionnement de frontal-nextcloud en tapant la ligne suivante à partir d'un nagivateur

```
http://localhost:3030
```
