import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { ServiceConfiguration } from 'meteor/service-configuration';

if (Meteor.settings.keycloak && Meteor.settings.public.keycloakUrl) {
  Accounts.config({
    forbidClientAccountCreation: true,
  });
  Accounts.onCreateUser(() => {
    throw new Meteor.Error('api.users.createUser', 'User creation is disabled');
  });
  ServiceConfiguration.configurations.upsert(
    { service: 'keycloak' },
    {
      $set: {
        loginStyle: 'redirect',
        serverUrl: Meteor.settings.public.keycloakUrl,
        realm: Meteor.settings.public.keycloakRealm,
        clientId: Meteor.settings.keycloak.client,
        realmPublicKey: Meteor.settings.keycloak.pubkey,
        bearerOnly: false,
      },
    },
  );
} else {
  console.log('! Error: Keycloak settings are mandatory !');
}
