import React from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import i18n from 'meteor/universe:i18n';
import getLang from '../../api/utils';
import App from '../../ui/layouts/App';

import '../locales';

/** Startup the application by rendering the App layout component. */
Meteor.startup(() => {
  i18n.setLocale(getLang());
  ReactDOM.render(<App />, document.getElementById('root'));
});
