import React from 'react';
import i18n from 'meteor/universe:i18n';
import Button from '@mui/material/Button';

/** Render a Not Found page if the user enters a URL that doesn't match any route. */

const notFoundStyle = {
  fontStyle: 'WorkSansRegular',
  textAlign: 'center',
  marginTop: '10%',
  overflowY: 'hidden',
  scrollBarWidth: 'none',
};

const linkStyle = {
  backgroundColor: 'transparent',
  border: 'none',
  cursor: 'pointer',
  textDecoration: 'underline',
  display: 'inline',
  margin: 0,
  padding: 0,
};

const { appName } = Meteor.settings.public;

export default function UserFailed() {
  return (
    <>
      <div style={notFoundStyle}>
        <h3 style={notFoundStyle}>
          {`${i18n.__('pages.UserFailed.loginMsg', { name: appName })} `}
          <button type="button" style={linkStyle} onClick={() => Meteor.loginWithKeycloak()}>
            {i18n.__('pages.UserFailed.loginLink')}
          </button>
        </h3>
        <Button
          variant="contained"
          style={{ fontStyle: 'WorkSansRegular' }}
          onClick={() => window.open(`${Meteor.settings.public.laboiteUrl}/signin`, '_blank')}
        >
          {i18n.__('pages.UserFailed.loginLaboite', { name: appName })}
        </Button>
      </div>
    </>
  );
}
