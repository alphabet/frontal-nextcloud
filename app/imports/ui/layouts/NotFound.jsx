import React from 'react';
import i18n from 'meteor/universe:i18n';
import Button from '@mui/material/Button';

/** Render a Not Found page if the user enters a URL that doesn't match any route. */

const notFoundStyle = {
  fontStyle: 'WorkSansRegular',
  textAlign: 'center',
  marginTop: '10%',
  overflowY: 'hidden',
  scrollBarWidth: 'none',
};
export default function NotFound() {
  const loggout = () => {
    const { keycloakUrl, keycloakRealm } = Meteor.settings.public;
    const keycloakLogoutUrl = `${keycloakUrl}/realms/${keycloakRealm}/protocol/openid-connect/logout`;
    const redirectUri = new URL('/?dologout', Meteor.absoluteUrl()).href;
    window.location = `${keycloakLogoutUrl}?post_logout_redirect_uri=${redirectUri}`;
  };
  return (
    <>
      <div style={notFoundStyle}>
        <h3 style={notFoundStyle}>{i18n.__('pages.NotFound.message')}</h3>

        <Button variant="contained" style={{ fontStyle: 'WorkSansRegular' }} onClick={() => loggout()}>
          {i18n.__('pages.NotFound.backButtonLabel')}
        </Button>
      </div>
    </>
  );
}
