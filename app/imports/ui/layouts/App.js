import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import NotFound from './NotFound';
import UserFailed from './UserFailed';

function App({ user, loggingIn, accountsConfigured }) {
  const params = new URL(document.location).searchParams;
  const dologout = params.has('dologout') === true;
  const [userFailed, setUserFailed] = useState(false);
  const [loggedOut, setLoggedOut] = useState(false);

  const formatURL = (url) => {
    if (url.startsWith('https://') || url.startsWith('http://')) return url;

    const finalUrl = `https://${url}`;
    return finalUrl;
  };

  useEffect(() => {
    // detect account creation failure (i.e: if logging in from keycloak)
    const stopCallback = Accounts.onLoginFailure((details) => {
      if (details.error.error === 'api.users.createUser') setUserFailed(true);
    });

    return () => {
      if (typeof stopCallback.stop === 'function') stopCallback.stop();
    };
  }, []);

  useEffect(() => {
    if (user) setUserFailed(false);
    if (!loggingIn && !user && !loggedOut && !userFailed && accountsConfigured) Meteor.loginWithKeycloak();
    if (user && user.nclocator) {
      setLoggedOut(true);
      Meteor.logout(() => {
        window.location = formatURL(user.nclocator);
      });
    }
  }, [loggingIn, user, accountsConfigured]);

  if (dologout) {
    Meteor.logout(() => {
      window.location = Meteor.absoluteUrl();
    });
  }

  return <>{userFailed ? <UserFailed /> : user && !user.nclocator ? <NotFound /> : null}</>;
}

App.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  loggingIn: PropTypes.bool.isRequired,
  accountsConfigured: PropTypes.bool.isRequired,
};

App.defaultProps = {
  user: {},
};

export default withTracker(() => {
  const user = Meteor.user();
  const loggingIn = Meteor.loggingIn();
  const accountsConfigured = Accounts.loginServicesConfigured();
  return {
    user,
    loggingIn,
    accountsConfigured,
  };
})(App);
