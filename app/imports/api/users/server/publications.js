import { Meteor } from 'meteor/meteor';

// automatically publish additional fields for current user
Meteor.publish(undefined, function publishUserData() {
  if (this.userId) {
    return Meteor.users.find(
      { _id: this.userId },
      {
        fields: { nclocator: 1 },
      },
    );
  }
  return this.ready();
});
